<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class GameController extends Controller
{
    public function create()
    {
        return view('game.create');
    }
    
    public function store(Request $request )
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);
        return redirect('/game');
    }

    //index
    public function index()
    {
        $post = DB::table('game')->get();
        return view('game.index', compact('post'));
    }
    //show
    public function show($id)
    {
        $post = DB::table('game')->where('id', $id)->first();
        return view('game.show', compact('post'));
    }

    //edit
    public function edit($id)
        {
            $post = DB::table('game')->where('id', $id)->first();
            return view('game.edit', compact('post'));
        }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $query = DB::table('game')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
            ]);
        return redirect('/game');
    }

    //delete
    public function destroy($id)
    {
        $query = DB::table('game')->where('id', $id)->delete();
        return redirect('/game');
    }
}

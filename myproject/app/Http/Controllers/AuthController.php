<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $nama1 = $request->first_name;
        $nama2 = $request->last_name;

        return view('halaman.welcome', compact('nama1','nama2'));
    }
}

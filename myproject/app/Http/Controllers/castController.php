<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class castController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }
    
    public function store(Request $request )
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    //index
    public function index()
    {
        $post = DB::table('cast')->get();
        return view('cast.index', compact('post'));
    }
    //show
    public function show($id)
    {
        $post = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('post'));
    }

    //edit
    public function edit($id)
        {
            $post = DB::table('cast')->where('id', $id)->first();
            return view('cast.edit', compact('post'));
        }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    //delete
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/form', 'AuthController@form');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-table', function ()
{
    return view('table.data-table');
});

Route::get('/table', function ()
{
    return view('table.table');
});

//crud cast
//create
Route::get('/cast/create','castController@create');
Route::post('/cast','castController@store');
//index
Route::get('/cast','castController@index');
Route::get('/cast/{cast_id}','castController@show');

Route::get('/cast/{cast_id}/edit','castController@edit');
Route::put('/cast/{cast_id}','castController@update');
Route::DELETE('/cast/{cast_id}','castController@destroy');

//tugas crud quiz
Route::get('/game/create','GameController@create');
Route::post('/game','GameController@store');
Route::get('/game','GameController@index');
Route::get('/game/{game}','GameController@show');
Route::get('/game/{game_id}/edit','GameController@edit');
Route::put('/game/{game_id}','GameController@update');
Route::DELETE('/game/{game_id}','GameController@destroy');

// crud film 
Route::resource('film','FilmController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

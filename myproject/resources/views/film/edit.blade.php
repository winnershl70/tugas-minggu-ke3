@extends('layout.master')

@section('judul')
    tambah film
@endsection

@section('content')

    
<div>
    <h2>Tambah Data</h2>
        <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" value="{{$film->judul}}" name="judul" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="ringkasan">ringkasan</label>
                <textarea class="form-control" value="{{$film->ringkasan}}" name="ringkasan" id="ringkasan" placeholder="Masukkan ringkasan" cols="30" rows="10"></textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tahun">tahun</label>
                <input type="text" class="form-control" value="{{$film->tahun}}" name="tahun" id="tahun" placeholder="Masukkan tahun">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="genre_id">genre</label>
                <select name="genre_id" id="genre_id" class="form-control" >
                    <option value="">---pilih genre---</option>
                    @foreach ($genre as $item)
                        @if ( $item->id === $film->genre_id)
                            
                        <option value="{{$item->id}}" selected>{{$item ->nama}}</option>
                        @else
                            
                        <option value="{{$item->id}}">{{$item ->nama}}</option>
                        @endif
                    
                    @endforeach  
                </select>
                @error('genre_id')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="poster">poster</label>
                <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan poster" >
                @error('poster')
                    <div class="alert alert-danger" >
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">edit</button>
        </form>
</div>

@endsection
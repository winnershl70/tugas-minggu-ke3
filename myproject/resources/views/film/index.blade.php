@extends('layout.master')

@section('judul')
    tambah film
@endsection

@section('content')
<a href="/film/create" class="btn btn-primary  my-2">tambah film</a>
<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('poster/'.$item->poster)}}" alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->judul}}</h2>
              <p class="card-text">{{$item->ringkasan}}</p>
             
              <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('DELETE') 
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-success btn-sm">edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="delete">
             </form>
            </div>
          </div>
    </div>
    @empty
        <h2>data kosong</h2>
    @endforelse
    
</div>
@endsection
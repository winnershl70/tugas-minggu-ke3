@extends('layout.master')

@section('judul')
Detail film id ke{{$film->id}}

@endsection

@section('content')
<img src="{{asset('poster/'.$film->poster)}}" alt="">
<h2>{{$film->judul}}</h2>
<p>{{$film->ringkasan}}</p>
<p>tahun pembuatan {{$film->tahun}}</p>
@endsection
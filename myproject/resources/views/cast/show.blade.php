@extends('layout.master')

@section('judul')
    halaman show
@endsection

@section('content')

<h2>Show Post {{$post->id}}</h2>
<h4>{{$post->nama}}</h4>
<p>{{$post->umur}}</p>
<p>{{$post->bio}}</p>

@endsection
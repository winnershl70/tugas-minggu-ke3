@extends('layout.master')

@section('judul')
    halaman edit
@endsection

@section('content')


<div>
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/game/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{$post->name}}" id="name" placeholder="Masukkan name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="gameplay">gameplay</label>
            <textarea class="form-control" name="gameplay" value="{{$post->gameplay}}" id="gameplay" placeholder="Masukkan gameplay" cols="30" rows="10"></textarea>
            @error('gameplay')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="developer">developer</label>
            <input type="text" class="form-control" name="developer"value="{{$post->developer}}"  id="developer" placeholder="Masukkan developer">
            @error('developer')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="year">year</label>
            <input type="text" class="form-control" name="year" value="{{$post->year}}" id="year" placeholder="Masukkan year">
            @error('year')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection
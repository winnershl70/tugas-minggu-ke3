@extends('layout.master')

@section('judul')
    halaman biodata
@endsection

@section('content')

    
<div>
    <h2>Tambah Data</h2>
        <form action="/game" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan name">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gameplay">gameplay</label>
                <textarea class="form-control" name="gameplay" id="gameplay" placeholder="Masukkan gameplay" cols="30" rows="10"></textarea>
                @error('gameplay')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">developer</label>
                <input type="text" class="form-control" name="developer" id="developer" placeholder="Masukkan developer">
                @error('developer')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="year">year</label>
                <input type="text" class="form-control" name="year" id="year" placeholder="Masukkan year">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection
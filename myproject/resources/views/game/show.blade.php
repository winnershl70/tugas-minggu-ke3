@extends('layout.master')

@section('judul')
    halaman show
@endsection

@section('content')

<h2>Show Post {{$post->id}}</h2>
<h4>{{$post->name}}</h4>
<p>{{$post->gameplay}}</p>
<p>{{$post->developer}}</p>
<p>{{$post->year}}</p>

@endsection
@extends('layout.master')

@section('judul')
    halaman biodata
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>

    <form action="/kirim" method="post">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="first_name">First name:</label><br><br>
            <input type="text" id="first_name" name="first_name"><br><br>
        <label for="last_name">Last name:</label><br><br>
            <input type="text" id="last_name" name="last_name"><br><br>
        <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="male"> Male <br>
            <input type="radio" name="gender" value="female"> Female <br>
            <input type="radio" name="gender" value="other"> Other <br><br>
        <label>Nationality:</label><br><br>
            <select>
                <option value="indonesia">Indonesia</option>
                <option value="amerika">Amerika</option>
                <option value="inggris">Inggris</option>
            </select><br><br>
        <label>Language Spoken:</label><br><br>
            <input type="checkbox" value="b_ind"> Bahasa Indonesia<br>
            <input type="checkbox" value="b_eng"> English<br>
            <input type="checkbox" value="b_other"> Other<br><br>
        <label>Bio:</label><br><br>
            <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection